/*

asphalt.js

1.0a2

A very simple framework-agnostic JavaScript data binding library.

(c) 2015 Ben Farhner, Farhner Manufacturing Company

*/
/*jslint white: true, plusplus: true */
/*global console, CustomEvent */

// new Asphalt()
// Creates a new instance of Asphalt.
// enableLogging: (optional) Whether or not to send log messages to the console. Defaults to false
var Asphalt = function (enableLogging) {
    'use strict';
    
    this.enableLogging = (enableLogging === true);
    this.Log('Warming up bitumen...');
};

Asphalt.prototype = {
    /*
     * Properties
     */
    
    Formatters: {},
    
    /*
     * Methods
     */
    
    // Constructor
    // new Asphalt()
    constructor: Asphalt,
    
    // Log()
    // Wrapper for console logging.
    Log: function (message) {
        'use strict';
        
        if (typeof console !== 'undefined' && this.enableLogging) {
            console.log('[Asphalt] ' + message);
        }
    },

    // GetArrayItemId()
    // Builds a unique ID for an array item given the binding path and array index.
    // Returns: string
    GetArrayItemId: function (bindingPath, index) {
        'use strict';
        
        return bindingPath.replace(/\./g, '-') + '-' + index;
    },

    // IsInputElement()
    // Determines whether or not the given element is a form input element.
    // Returns: bool
    IsInputElement: function (element) {
        'use strict';
        
        if (typeof element !== 'undefined' && element !== null &&
            (element.tagName === 'INPUT' || element.tagName === 'SELECT')) {
            return true;
        }
        
        return false;
    },
    
    // IsBoolInputElement()
    // Determines whether or not the given element is a boolean-based form input element, i.e. a radio button or checkbox.
    // Returns: bool
    IsBoolInputElement: function (element) {
        'use strict';
        
        if (typeof element !== 'undefined' && element !== null && element.tagName === 'INPUT' &&
            (element.type === 'radio' || element.type === 'checkbox')) {
            return true;
        }
        
        return false;
    },

    // GetPropertyFromPath()
    // Gets the property name from the given binding path.
    // Returns: string
    GetPropertyFromPath: function (bindingPath) {
        'use strict';
        
        var pathParts;
        
        bindingPath = bindingPath || '';
        pathParts = bindingPath.split('.');
        
        return pathParts[pathParts.length - 1];
    },

    // GetBindingObject()
    // Finds the object in the given model containing the bound property in the given binding path.
    // Returns: object
    GetBindingObject: function (model, bindingPath) {
        'use strict';
        
        var pathParts,
            bindingObject,
            index;
        
        if (typeof model === 'undefined' || model === null) {
            return null;
        }
        
        bindingPath = bindingPath || '';
        
        // Split the path by dot notation
        pathParts = bindingPath.split('.');
        
        // Initially point to the model itself
        bindingObject = model;
        
        // Loop over property parts to find the property in the model
        for (index = 0; index < pathParts.length; index++) {
            if (bindingObject.hasOwnProperty(pathParts[index])) {
                // Only travel down to the parent object, not the actual binding value
                if (index < pathParts.length - 1) {
                    bindingObject = bindingObject[pathParts[index]];
                }
            }
            else {
                return null;
            }
        }
        
        return bindingObject;
    },
    
    // GetFormattedValue()
    // Returns the given value, formatted by the given element's formatter if available
    GetFormattedValue: function (element, value) {
        'use strict';
        
        var formatterName,
            formatter;

        if (typeof element === 'undefined' || element === null ||
            typeof value === 'undefined' || value === null) {
            return null;
        }
        
        // Check for formatter
        if (element.hasAttribute('data-bind-formatter')) {
            formatterName = element.getAttribute('data-bind-formatter');

            if (this.Formatters.hasOwnProperty(formatterName)) {
                value = this.Formatters[formatterName](value);
            }
            else {
                this.Log('Invalid data-bind-formatter: "' + formatterName + '"');
            }
        }
        
        return value;
    },
    
    // GetFormattedOptionValue()
    // Returns the given value, formatted by the given element's value formatter if available
    GetFormattedOptionValue: function (element, value) {
        'use strict';
        
        var formatterName,
            formatter;

        if (typeof element === 'undefined' || element === null ||
            typeof value === 'undefined' || value === null) {
            return null;
        }
        
        // Check for formatter
        if (element.hasAttribute('data-bind-value-formatter')) {
            formatterName = element.getAttribute('data-bind-value-formatter');

            if (this.Formatters.hasOwnProperty(formatterName)) {
                value = this.Formatters[formatterName](value);
            }
            else {
                this.Log('Invalid data-bind-value-formatter: "' + formatterName + '"');
            }
        }
        
        return value;
    },

    // SetElementValue()
    // Sets either the text value or field value of the given DOM element.
    SetElementValue: function (element, value) {
        'use strict';
        
        var target,
            formattedValue;

        if (typeof element === 'undefined' || element === null ||
            typeof value === 'undefined' || value === null) {
            return;
        }
        
        formattedValue = this.GetFormattedValue(element, value);
        
        // Check for binding target if available, otherwise default to 'text'
        target = element.getAttribute('data-bind-target') || 'text';
        
        if (target === 'text') {
            if (this.IsBoolInputElement(element)) {
                // Special case for checkboxes and radio buttons
                element.checked = Boolean(formattedValue);
            }
            else if (this.IsInputElement(element)) {
                element.value = formattedValue;
            }
            else {
                element.textContent = formattedValue;
            }
        }
        else if (target === 'html') {
            element.innerHTML = value;
        }
        else if (element.style.hasOwnProperty(target)) {
            element.style[target] = formattedValue;
        }
        else {
            this.Log('Invalid data-bind-target: "' + target + '"');
        }
    },

    // SetSelectOptions()
    // For <select> inputs, checks for provided source and binds that to the dropdown options.
    SetSelectOptions: function (element, model) {
        'use strict';
        
        var that = this,
            bindingPath,
            bindingObject,
            bindingProperty,
            bindingArray,
            optionElement,
            index;

        if (typeof element === 'undefined' || element === null ||
            typeof model === 'undefined' || model === null) {
            return;
        }

        if (element.tagName === 'SELECT' && element.hasAttribute('data-bind-options')) {
            bindingPath = element.getAttribute('data-bind-options') || '';
            bindingObject = this.GetBindingObject(model, bindingPath);

            if (bindingObject === null) {
                this.Log('Could not bind options to "' + bindingPath + '": it does not exist in the model!');

                return false;
            }

            bindingProperty = this.GetPropertyFromPath(bindingPath);
            bindingArray = bindingObject[bindingProperty];

            // Remove any existing elements
            while (element.hasChildNodes()) {
                element.removeChild(element.firstChild);
            }

            // Loop over all the items in the array
            for (index = 0; index < bindingArray.length; index++) {
                optionElement = document.createElement('option');                
                optionElement.value = this.GetFormattedOptionValue(element, bindingArray[index]);
                optionElement.textContent = this.GetFormattedValue(element, bindingArray[index]);
                element.appendChild(optionElement);
            }
        }
    },

    // BindArrayItemElement()
    // Binds a specific array item to the given model using the given template
    // template: The <template> used to build out the array item
    // model: The array item object
    // arrayIndex: The index of the array item in its containing array
    // insertBeforeElement: The element before which to insert the templated item. A null value will place the templated item at the end.
    BindArrayItemElement: function (template, model, arrayIndex, insertBeforeElement) {
        'use strict';
        
        var arrayItemElement,
            boundElements,
            index,
            bindingPath,
            id;

        if (typeof template === 'undefined' || template === null ||
            typeof model === 'undefined' || model === null) {
            return null;
        }

        // Clone the template
        arrayItemElement = document.importNode(template.content, true);
        
        boundElements = arrayItemElement.querySelectorAll('[data-bind]');
        
        // Loop over each bound element in the template and bind it
        for (index = 0; index < boundElements.length; index++) {
            this.BindElement(boundElements[index], model);
        }
        
        bindingPath = template.getAttribute('data-bind') || '';
        id = this.GetArrayItemId(bindingPath, arrayIndex);
        
        // Set template GUID
        if (arrayItemElement.firstElementChild !== null) {
            arrayItemElement.firstElementChild.id = id;
        }
        
        // Insert the templated item into the DOM
        template.parentElement.insertBefore(arrayItemElement, insertBeforeElement);
        
        // Trigger custom event for further processing on the templated item
        document.dispatchEvent(new CustomEvent('Asphalt.NewArrayItem', { 'detail': id }));
    },

    // BindElement()
    // Binds the given DOM element using the given model
    // element: DOM element to which to bind
    // model: Model to bind to the DOM element
    BindElement: function (element, model) {
        'use strict';
        
        var that = this,
            bindingPath,
            bindingObject,
            bindingProperty,
            bindingArray,
            index;

        if (typeof element === 'undefined' || element === null ||
            typeof model === 'undefined' || model === null) {
            return false;
        }

        bindingPath = element.getAttribute('data-bind') || '';
        bindingObject = this.GetBindingObject(model, bindingPath);

        if (bindingObject === null) {
            this.Log('Could not bind to "' + bindingPath + '": it does not exist in the model!');

            return false;
        }

        bindingProperty = this.GetPropertyFromPath(bindingPath);
        
        // Check if we're templating out an array
        if (element.tagName === 'TEMPLATE' && Array.isArray(bindingObject[bindingProperty])) {
            bindingArray = bindingObject[bindingProperty];
            
            // Loop over all the items in the array
            for (index = 0; index < bindingArray.length; index++) {
                this.BindArrayItemElement(element, bindingArray[index], index, null);
            }

            // Observe future changes to the array
            Array.observe(bindingArray, function (changes) {
                changes.forEach(function (change) {
                    var index,
                        insertBeforeElement = null;
                    
                    // Remove all removed items
                    for (index = 0; index < change.removed.length; index++) {
                        document.getElementById(that.GetArrayItemId(bindingPath, change.index + index)).remove();
                    }

                    // Get element at the splice index after which to insert new items
                    if (change.index > 0) {
                        insertBeforeElement = document.getElementById(that.GetArrayItemId(bindingPath, change.index - 1)).nextElementSibling;
                    }

                    // Add all added items
                    for (index = 0; index < change.addedCount; index++) {
                        that.BindArrayItemElement(element, change.object[change.index + index], change.index + index, insertBeforeElement);
                    }
                });
            }, ['splice']);
        }
        else {
            // Set select source if needed
            this.SetSelectOptions(element, model);

            // Set initial value on the element
            this.SetElementValue(element, bindingObject[bindingProperty]);
            
            // Set value again when it loses focus
            element.addEventListener('blur', function (event) {
                that.SetElementValue(element, bindingObject[bindingProperty]);
            });

            if (this.IsBoolInputElement(element)) {
                // Listen for changes to the input
                element.addEventListener('change', function (event) {
                    bindingObject[bindingProperty] = this.checked;
                });
                element.addEventListener('keyup', function (event) {
                    bindingObject[bindingProperty] = this.checked;
                });
            }
            else if (this.IsInputElement(element)) {
                // Listen for changes to the input
                element.addEventListener('change', function (event) {
                    bindingObject[bindingProperty] = this.value;
                });
                element.addEventListener('keyup', function (event) {
                    bindingObject[bindingProperty] = this.value;
                });
            }
            
            // Observe future changes to the value
            Object.observe(bindingObject, function (changes) {
                changes.forEach(function (change) {
                    if (change.name === bindingProperty && !document.activeElement.isEqualNode(element)) {
                        that.SetElementValue(element, change.object[change.name]);
                    }
                });
            }, ['update']);
        }
    },
    
    // Bind()
    // Binds the given model to the UI.
    Bind: function (model) {
        'use strict';
        
        var boundElements,
            index;

        if (typeof model === 'undefined' || model === null) {
            return;
        }

        this.Log('Pouring pavement...');

        // Grab all bound elements
        boundElements = document.querySelectorAll('[data-bind]');

        // Loop over each bound element and bind it
        for (index = 0; index < boundElements.length; index++) {
            this.BindElement(boundElements[index], model);
        }

        this.Log('Pavement has cooled.');
    },
    
    // AddFormatter()
    // Adds a new formatter that can be used with data-bind-formatter.
    AddFormatter: function (name, formatter) {
        'use strict';
        
        if (typeof name !== 'string' || name === null || name.trim() === '' ||
            typeof formatter !== 'function' || formatter === null) {
            return false;
        }
        
        // Add formatter to list of available formatters
        this.Formatters[name] = formatter;
        
        this.Log('Added formatter "' + name + '"');
        
        return true;
    }
};
