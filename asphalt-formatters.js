/*

asphalt-formatters.js

A basic set of built-in formatters. Include this immediately after asphalt.js.

(c) 2015 Ben Farhner, Farhner Manufacturing Company

*/
/*jslint white: true, plusplus: true */
/*global Asphalt */

Asphalt.prototype.Formatters = {
    // Formats a value as US currency: $0.00
    currency: function (value) {
        'use strict';
        
        var floatValue = parseFloat(value);
        
        if (Number.isNaN(floatValue)) {
            floatValue = 0;
        }
        
        return '$' + floatValue.toFixed(2);
    }
};
